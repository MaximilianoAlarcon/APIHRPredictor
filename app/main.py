from flask import Flask, request, jsonify
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import requests, random, string, pandas as pd, os
import cv2,numpy as np
from app.jeanCV import skinDetector
from app.functions import *

app = Flask(__name__)
#API throttling, prevents spam and hacking
limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=["200 per day", "50 per hour"]
)



faceCascade = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_alt0.xml")
faceCascade_profileface = cv2.CascadeClassifier("haarcascades/haarcascade_profileface.xml")
faceCascade_default = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_default.xml")
faceCascade1 = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_alt1.xml")
faceCascade2 = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_alt2.xml")


def try_others_scales(face_rects,img,alphas,betas):
	for a in alphas:
		for b in betas:
		    img_transformada = cv2.convertScaleAbs(img, alpha=a, beta=b)
		    gray = cv2.cvtColor(img_transformada, cv2.COLOR_RGB2GRAY)
		    face_rects = faceCascade.detectMultiScale(gray, scale_factor, minneighbours) 
		    if len(face_rects) == 1:
		      break;
		    face_rects = faceCascade_default.detectMultiScale(gray, scale_factor, minneighbours) 
		    if len(face_rects) == 1:
		      break;
		    face_rects = faceCascade1.detectMultiScale(gray, scale_factor, minneighbours) 
		    if len(face_rects) == 1:
		      break;
		    face_rects = faceCascade2.detectMultiScale(gray, scale_factor, minneighbours) 
		    if len(face_rects) == 1:
		      break;

def extract_layer_as_list(layer):
	pixeles = []
	for a in layer:
		if a > 0:
		  pixeles.append(a)
	return pixeles

def extract_data_from_frames(cap):
	column_min_capa_verde = []
	column_max_capa_verde = []
	column_mean_capa_verde = []
	column_std_capa_verde = []

	alphas = [1]
	betas = [100]
	scale_factor = 1.3
	minneighbours = 5
	len_total = len(cap)
	index = 0
	for img in cap:
		gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
		roi_frame = img
		face_rects = faceCascade.detectMultiScale(gray, scale_factor, minneighbours)
		if len(face_rects) != 1:
		    try_others_scales(face_rects,img,alphas,betas)
		if len(face_rects) == 1:
		    for (x, y, w, h) in face_rects:
		        roi_frame = img[y:y + h, x:x + w]
		    detector = skinDetector(cv2.cvtColor(np.array(roi_frame), cv2.COLOR_BGR2RGB))
		    roi_frame = detector.find_skin()
		    roi_frame = roi_frame[:,:,:]
		    frame = np.ndarray(shape=roi_frame.shape, dtype="float")
		    frame[:] = roi_frame * (1./255)
		    capa_verde = np.zeros((frame.shape[0],frame.shape[1],1))
		    capa_verde[:,:,0] = frame[:,:,1]
		    capa_verde = capa_verde.reshape(frame.shape[0]*frame.shape[1])
		    del frame;
		    pixeles = extract_layer_as_list(capa_verde)
		    min = np.min(pixeles)*255
		    max = np.max(pixeles)*255
		    mean = np.mean(pixeles)*255
		    std = np.std(pixeles)*255
		    column_min_capa_verde.append(min)
		    column_max_capa_verde.append(max)
		    column_mean_capa_verde.append(mean)
		    column_std_capa_verde.append(std)
	return column_min_capa_verde,column_max_capa_verde,column_mean_capa_verde,column_std_capa_verde



@app.route("/prediction", methods = ['POST'])
@limiter.limit("1/second", override_defaults=False)
@limiter.limit("10/minute", override_defaults=False)
def predict_signal_hr():
	status = ""
	signal = []
	message = ""
	data = request.get_json(force=True)
	if "URL_VIDEO" in data:
		cap = []
		vcap = cv2.VideoCapture(data["URL_VIDEO"])
		while(True):
		    ret, frame = vcap.read()
		    if frame is not None:
		        cap.append(frame)
		    else:
		        break

		if len(cap) == 0:
			status = "failed"
			message = "We could not read the video"
			return jsonify({"status":status,"message":message})

		column_min_capa_verde,column_max_capa_verde,column_mean_capa_verde,column_std_capa_verde = extract_data_from_frames(cap)

		if len(column_min_capa_verde) == 0:
			status = "failed"
			message = "We could not detect a face"
			return jsonify({"status":status,"message":message})
		df_res = pd.DataFrame()
		df_res["min_capa_verde"] = pd.Series(column_min_capa_verde)
		df_res["max_capa_verde"] = pd.Series(column_max_capa_verde)
		df_res["mean_capa_verde"] = pd.Series(column_mean_capa_verde)
		df_res["std_capa_verde"] = pd.Series(column_std_capa_verde)    
		min, max, mean, std, pred = predecir_senial_hr(df_res)

		status = "success"
		message = "The prediction was successful"
		return jsonify({"status":status,"message":message,"signal":pred.tolist()})
	else:
		status = "failed"
		message = "You have to fill the field 'URL_VIDEO' in the body"
		return jsonify({"status":status,"message":message})


@app.route('/')
def index():
  return "<h1>ML API Service</h1>"

#The app is running in a public IP
#if __name__ == "__main__":
#	app.run(host='0.0.0.0', port=8050, debug=False)